package com.example.demo.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.AnswerDto;
import com.example.demo.dto.QuestionDto;
import com.example.demo.dto.UserDto;
import com.example.demo.dto.VoteDto;

@Repository
public class ViewRepository {
	
	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Cacheable("users")
	public List<UserDto> getUserData(String name, String email, Long minimumScore) {
		
		//TODO minimumScore
		
		List<UserDto> dtos = new ArrayList<>();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("name", name)
				.addValue("email", email)
				.addValue("minimumScore", minimumScore);
		
		namedParameterJdbcTemplate.query(
	        "SELECT "
	        + "	u.id, "
	        + "	u.name, "
	        + "	u.email "
	        + "	FROM users.user u "
	        + " INNER JOIN users.reputation_user ru on ru.id_user = u.id "
	        + "	WHERE u.enabled = 1 "
	        + "	AND ( :name is null OR u.name = :name ) "
	        + "	AND ( :email is null OR u.email = :email ) "
	        + "	AND ( :minimumScore is null OR ru.score >= :minimumScore ) ", 
	        parameters, 
	        (rs, rowNum) -> new UserDto(rs.getLong("id"), rs.getString("name"), rs.getString("email"))
	        
	    ).forEach(user -> dtos.add(populateUserData(user)));
	    return dtos;
	}

	private UserDto populateUserData(UserDto user) {
		user.setQuestions(getQuestionDataByUser(user.getIdUser()));
		user.setAnswers(getAnswerDataByUser(user.getIdUser()));
		user.setVotes(getVoteDataByUser(user.getIdUser()));
		return user;
	}
	
	private List<QuestionDto> getQuestionDataByUser(Long idUser) {
		List<QuestionDto> dtos = new ArrayList<>();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("id_user", idUser);
		
		namedParameterJdbcTemplate.query(
	        "SELECT "
	        + "	q.id, "
	        + "	q.id_user, "
	        + "	q.comment "
	        + "	FROM questions.question q "
	        + "	WHERE q.id_user = :id_user", 
	        parameters, 
	        (rs, rowNum) -> new QuestionDto(rs.getLong("id"), rs.getLong("id_user"), rs.getString("comment"))
	        
	    ).forEach(question -> dtos.add(question));
	    return dtos;
	}
	
	private List<AnswerDto> getAnswerDataByUser(Long idUser) {
		List<AnswerDto> dtos = new ArrayList<>();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("id_user", idUser);
		
		namedParameterJdbcTemplate.query(
	        "SELECT "
	        + "	a.id, "
	        + "	a.id_user, "
	        + "	a.id_question, "
	        + "	a.comment "
	        + "	FROM answers.answer a "
	        + "	WHERE a.id_user = :id_user", 
	        parameters, 
	        (rs, rowNum) -> new AnswerDto(rs.getLong("id"), rs.getLong("id_user"), rs.getLong("id_question"), rs.getString("comment"))
	        
	    ).forEach(answer -> dtos.add(answer));
	    return dtos;
	}
	
	private List<VoteDto> getVoteDataByUser(Long idUser) {
		List<VoteDto> dtos = new ArrayList<>();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("id_user", idUser);
		
		namedParameterJdbcTemplate.query(
	        "SELECT "
	        + "	v.id, "
	        + "	v.id_answer, "
	        + "	v.id_user, "
	        + "	v.score "
	        + "	FROM votes.vote v "
	        + "	WHERE v.id_user = :id_user", 
	        parameters, 
	        (rs, rowNum) -> new VoteDto(rs.getLong("id"), rs.getLong("id_answer"), rs.getLong("id_user"), rs.getLong("score"))
	        
	    ).forEach(vote -> dtos.add(vote));
	    return dtos;
	}
}
