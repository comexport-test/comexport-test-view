package com.example.demo.dto;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class VoteDto {
	
	@NonNull
	Long id;
	
	@NonNull
	Long idAnswer;
	
	@NonNull
	Long idUser;
	
	@NonNull
	Long score;
	
	LocalDateTime createdAt;
	LocalDateTime updatedAt;
}
