package com.example.demo.dto;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class AnswerDto {
	
	@NonNull
	Long idAnswer;
	
	@NonNull
	Long idUser;
	
	@NonNull
	Long idQuestion;
	
	@NonNull
	String comment;
	
	LocalDateTime createdAt;
	LocalDateTime updatedAt;
}
