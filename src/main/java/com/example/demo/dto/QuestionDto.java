package com.example.demo.dto;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class QuestionDto {
	
	@NonNull
	Long id;
	
	@NonNull
	Long idUser;
	
	List<FlagDto> flags;
	
	@NonNull
	String comment;
	
	Boolean resolved;
	
	LocalDateTime createdAt;
	LocalDateTime updatedAt;
}
