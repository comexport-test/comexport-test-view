package com.example.demo.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class RoleDto {
	
	Long id;
	String description;
	LocalDateTime createdAt;
	LocalDateTime updatedAt;
}
