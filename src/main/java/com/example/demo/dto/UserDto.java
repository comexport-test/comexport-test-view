package com.example.demo.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class UserDto {
	
	@NonNull
	Long idUser;
	
	@NonNull
	String name;
	
	@NonNull
	String email;
	
	List<QuestionDto> questions;
	List<AnswerDto> answers;
	List<VoteDto> votes;
	
}
