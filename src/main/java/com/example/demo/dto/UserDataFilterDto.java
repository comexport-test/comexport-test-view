package com.example.demo.dto;

import lombok.Data;

@Data
public class UserDataFilterDto {
	Integer page;
	Integer size;
	String email;
	String name;
	String questionKeyword;
	String answerKeyword;
	Long mostUpvoted;
	Long userReputationScore;

}
