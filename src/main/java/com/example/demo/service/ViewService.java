package com.example.demo.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.AnswerDto;
import com.example.demo.dto.QuestionDto;
import com.example.demo.dto.UserDataFilterDto;
import com.example.demo.dto.UserDto;
import com.example.demo.repository.ViewRepository;

@Service
public class ViewService {
	
	private final Logger log = LoggerFactory.getLogger(ViewService.class);
	
	@Autowired
	ViewRepository demoRepository;

	public List<UserDto> find(UserDataFilterDto filter) {
		List<UserDto> dtos = demoRepository.getUserData(filter.getName(), filter.getEmail(), filter.getUserReputationScore());
		List<UserDto> paginated = resolvePagination(dtos, filter.getPage(), filter.getSize());
		List<UserDto> filtered = resolveFilters(paginated, filter.getQuestionKeyword(), filter.getAnswerKeyword());
		return filtered;
	}

	private List<UserDto> resolvePagination(List<UserDto> dtos, Integer page, Integer size) {
		return dtos.stream().skip(page * size).limit(size).collect(Collectors.toList());
	}
	
	private List<UserDto> resolveFilters(List<UserDto> paginated, String questionKeyword, String answerKeyword) {
		
		return paginated.stream()
				.filter(user -> validateUser(user, questionKeyword, answerKeyword))
				.collect(Collectors.toList());
	}

	private boolean validateUser(UserDto user, String questionKeyword, String answerKeyword) {
		return validateQuestions(user, questionKeyword) && validateAnswers(user, answerKeyword);
	}

	private boolean validateQuestions(UserDto user, String questionKeyword) {
		if(questionKeyword != null) {
			List<QuestionDto> filteredByQuestionKeyWord = user.getQuestions().stream()
					.filter(question -> questionKeyword == null || question.getComment().contains(questionKeyword))
					.collect(Collectors.toList());
			
			if(filteredByQuestionKeyWord.size() < 1) {
				return false;
			}
		}
		return true;
	}
	
	private boolean validateAnswers(UserDto user, String answerKeyword) {
		if(answerKeyword != null) {
			List<AnswerDto> filteredByAnswerKeyWord = user.getAnswers().stream()
					.filter(answer -> answerKeyword == null || answer.getComment().contains(answerKeyword))
					.collect(Collectors.toList());
			
			if(filteredByAnswerKeyWord.size() < 1) {
				return false;
			}
		}
		return true;
	}

}
